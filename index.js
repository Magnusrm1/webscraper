const puppeteer = require('puppeteer');
const fs = require("fs");

async function scrape() {
    try {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        await page.goto("https://liquipedia.net/starcraft2/Main_Page");

        const tournaments = await page.evaluate( () => {

            const grabFromListItem = (listItem, classname) => listItem
                .querySelector(classname)
                .innerText
                .trim();

            const data = [];

            const tournamentItems = document.querySelectorAll("ul.tournaments-list-type-list > li > a");
            const myList = Array.from(tournamentItems).map( e => {return e});
            
            myList.map(item => {
                data.push({
                    tournamentName: grabFromListItem(item, "span.tournaments-list-name"),
                    tournamentDates: grabFromListItem(item, "small.tournaments-list-dates")
                });
            });
            return data;
        });

        console.log(JSON.stringify(tournaments, null, 2 ));

        fs.writeFile(
            './json/tournaments.json',
            JSON.stringify(tournaments, null, 2 ),
            (err) => err ? console.error("Data not written!", err) : console.log("Data written!")
        )

    } catch (e) {
        console.log(e);
    }
}
scrape();